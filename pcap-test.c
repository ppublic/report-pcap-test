#include <arpa/inet.h>
#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>

void usage()
{
    printf("syntax: pcap-test <interface>\n");
    printf("sample: pcap-test wlan0\n");
}

// Print MAC Address
void printMac(uint8_t *mac)
{
    printf("%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

// Print IPv4 Address
void printIPv4(struct in_addr ip_address)
{
    char char_ip_address[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &ip_address, char_ip_address, INET_ADDRSTRLEN);
    printf("%s", char_ip_address);
}

// Print Port Number
void printPort(u_int16_t port)
{
    printf("%d", (unsigned int)ntohs(port));
}

// Print TCP Payload
void printPayload(const u_char *packet, int length)
{
    const int line_size = 16;
    const int buf_size = 3 * line_size + (line_size / 2) + 2 + line_size + 1;
    char output_line[buf_size];

    for (int i = 0; i < length; i += line_size)
    {
        char *buf_ptr = output_line;
        int line_end = (i + line_size <= length) ? (i + line_size) : length;

        for (int j = i; j < line_end; ++j)
        {
            buf_ptr += sprintf(buf_ptr, "%02X", packet[j]);
            if ((j + 1) % 2 == 0)
            {
                buf_ptr += sprintf(buf_ptr, " ");
            }
        }
        for (int k = line_end; k < i + line_size; ++k)
        {
            buf_ptr += sprintf(buf_ptr, "  ");
            if ((k + 1) % 2 == 0)
            {
                buf_ptr += sprintf(buf_ptr, " ");
            }
        }

        buf_ptr += sprintf(buf_ptr, " ");
        for (int j = i; j < line_end; ++j)
        {
            buf_ptr += sprintf(buf_ptr, "%c", (packet[j] >= 32 && packet[j] <= 126) ? packet[j] : '.');
        }
        printf("\t\t%s\n", output_line);
    }
    printf("\n");
}

typedef struct
{
    char *dev_;
} Param;

Param param = {
    .dev_ = NULL};

bool parse(Param *param, int argc, char *argv[])
{
    if (argc != 2)
    {
        usage();
        return false;
    }
    param->dev_ = argv[1];
    return true;
}

// Ethernet Header Structure(from libnet-headers.h)
#define ETHER_ADDR_LEN 6
struct libnet_ethernet_hdr
{
    u_int8_t ether_dhost[ETHER_ADDR_LEN]; /* destination ethernet address */
    u_int8_t ether_shost[ETHER_ADDR_LEN]; /* source ethernet address */
    u_int16_t ether_type;                 /* protocol */
};

// IPv4 Header  Structure(from libnet-headers.h)
struct libnet_ipv4_hdr
{
#if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__) // Modify due to "has no member named" error
    u_int8_t ip_hl : 4,                         /* header length */
        ip_v : 4;                               /* version */
#elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)  // Modify due to "has no member named" error
    u_int8_t ip_v : 4,   /* version */
        ip_hl : 4;       /* header length */
#endif
    u_int8_t ip_tos; /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY 0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT 0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY 0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST 0x02
#endif
    u_int16_t ip_len; /* total length */
    u_int16_t ip_id;  /* identification */
    u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000 /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000 /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000 /* more fragments flag */
#endif
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff /* mask for fragmenting bits */
#endif
    u_int8_t ip_ttl;               /* time to live */
    u_int8_t ip_p;                 /* protocol */
    u_int16_t ip_sum;              /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
};

// TCP Header Structure(from libnet-headers.h)
struct libnet_tcp_hdr
{
    u_int16_t th_sport; /* source port */
    u_int16_t th_dport; /* destination port */
    u_int32_t th_seq;   /* sequence number */
    u_int32_t th_ack;   /* acknowledgement number */

#if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__) //  Modify due to "has no member named" error
    u_int8_t th_x2 : 4,                         /* (unused) */
        th_off : 4;                             /* data offset */
#elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)  // Modify due to "has no member named" error
    u_int8_t th_off : 4, /* data offset */
        th_x2 : 4;       /* (unused) */
#endif
    u_int8_t th_flags; /* control flags */
#ifndef TH_FIN
#define TH_FIN 0x01 /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN 0x02 /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST 0x04 /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH 0x08 /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK 0x10 /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG 0x20 /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE 0x40
#endif
#ifndef TH_CWR
#define TH_CWR 0x80
#endif
    u_int16_t th_win; /* window */
    u_int16_t th_sum; /* checksum */
    u_int16_t th_urp; /* urgent pointer */
};

int main(int argc, char *argv[])
{
    if (!parse(&param, argc, argv))
        return -1;

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
    if (pcap == NULL)
    {
        fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
        return -1;
    }

    while (true)
    {
        struct pcap_pkthdr *header;
        const u_char *packet;
        unsigned long accumulated_packet_size = 0;
        time_t raw_time;
        struct tm *time_info;

        int res = pcap_next_ex(pcap, &header, &packet);
        if (res == 0)
            continue;
        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
        {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
            break;
        }

        // Parse Ethernet Header
        struct libnet_ethernet_hdr *eth_hdr = (struct libnet_ethernet_hdr *)packet;

        // If Ethernet Type isn't IPv4 -> continue
        if (ntohs(eth_hdr->ether_type) != 0x0800)
            continue;

        // Ehternet Header Size
        accumulated_packet_size += sizeof(struct libnet_ethernet_hdr);

        // Parse IPv4 Header
        struct libnet_ipv4_hdr *ipv4_hdr = (struct libnet_ipv4_hdr *)(packet + accumulated_packet_size);

        // If IPv4 Type isn't TCP -> cotinue
        if (ipv4_hdr->ip_p != 0x6)
            continue;

        // Ehternet Header Size + IP Header Size
        accumulated_packet_size += sizeof(struct libnet_ipv4_hdr);

        // Parse TCP Header
        struct libnet_tcp_hdr *tcp_hdr = (struct libnet_tcp_hdr *)(packet + accumulated_packet_size);

        // Ehternet Header Size + IPv4 Header Size + TCP/IP Header Size
        accumulated_packet_size += sizeof(struct libnet_tcp_hdr);

        // Get current time
        time(&raw_time);
        time_info = localtime(&raw_time);
        char time_buffer[80];
        strftime(time_buffer, sizeof(time_buffer), "%Y-%m-%d %H:%M:%S", time_info);

        // Print adresses and TCP payload
        printf("[%s] %u bytes captured\n", time_buffer, header->caplen);
        printf("\tMac : ");
        printMac(eth_hdr->ether_shost);
        printf(" -> ");
        printMac(eth_hdr->ether_dhost);
        printf("\n");

        printf("\tIPv4: ");
        printIPv4(ipv4_hdr->ip_src);
        printf(" -> ");
        printIPv4(ipv4_hdr->ip_dst);
        printf("\n");

        printf("\tPort: ");
        printPort(tcp_hdr->th_sport);
        printf(" -> ");
        printPort(tcp_hdr->th_dport);
        printf("\n");

        // TCP Payload Length = IPv4 Total Length - IPv4 Header Length - TCP Header Length
        uint16_t tcp_payload_length = ntohs(ipv4_hdr->ip_len) - (ipv4_hdr->ip_hl * 4) - (tcp_hdr->th_off * 4);
        printf("\tTCP Payload [%d bytes]: \n", tcp_payload_length);
        printPayload(packet + accumulated_packet_size, 10); // Print payload of 10 bytes
        // printPayload(packet + accumulated_packet_size, tcp_payload_length); // Print payload
    }
    pcap_close(pcap);
}